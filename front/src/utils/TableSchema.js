export const TableSchemes = [
  {
    path: "school",
    title: "Schools",
    table_headers: ["学校名称", "地址", "学校联系电话"],
    table_body: ["schoolName", "address", "phoneNumber"],
  },
  {
    path: "student",
    title: "Students",
    table_headers: ["学生姓名", "年级", "学校名称"],
    table_body: ["studentName", "gradeLevel", "school.schoolName"],
  },
];
