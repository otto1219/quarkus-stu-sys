/* eslint-disable react/prop-types */

import { HeaderTwo } from './HeaderTwo'


const Content = ({ sidebarHandler }) => {
  return (
    <div className="content">
      <HeaderTwo sidebarHandler={sidebarHandler} />
    </div>
  )
}

export default Content