import { useRef, useState } from 'react';
import '../styles/Login.css'
import { useNavigate } from 'react-router-dom';
import { EyeIcon } from '../assets/icons/Icons';
import loginFetch from '../utils/Login.js'
import Cookies from 'js-cookie'
import { toast } from 'sonner';

export const Login = () => {

  const navigate = useNavigate();

  const email = useRef()
  const password = useRef()

  const [passwordViewer, setPasswordViewer] = useState(false);

  const handleSubmit = async () => {
    navigate("/users")
    loginFetch({ username: email.current.value, password: password.current.value })
      .then((response) => {
        if (response.status === 200) {
          toast.success('登录成功')

        }
        if (response.status === 401) toast.error('没有权限');
        return response.json()
      })
      .then((data) => {
        if (data.token) { Cookies.set('token', data.token) }
      })
      .catch((error) => { toast.error('登录失败'); console.log(error) });
  }

  const handleCheckboxChange = () => {
    setPasswordViewer(!passwordViewer)
  }

  return (
    <div className='login-component'>
      <div className='login'>
        <div className="login-card">
          <div className='login-icon'>
            <h2>otto的信息管理系统</h2>
            <span>请输入您的账号和密码</span>
          </div>
          <div className="login-elements">
            <span>用户</span>
            <input type="text" placeholder="输入您的用户名..." ref={email} />
            <span>密码</span>
            <div className='password-input'>
              <input
                type={passwordViewer ? 'text' : 'password'}
                placeholder="输入您的密码"
                ref={password}
              />
              <EyeIcon action={handleCheckboxChange} />
            </div>
          </div>
          <button className="login-button" onClick={handleSubmit}>登录</button>
          <p>如果您没有账户用于登录 <strong> 请联系</strong></p>
        </div>
      </div>
    </div>
  )
}