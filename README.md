# 基于 React 和 Quarkus 的全栈项目

此仓库包含一个全栈项目，前端使用 React，后端使用 Quarkus。数据库使用postgres

## 项目结构

```
├── back
│ ├── .idea
│ ├── .mvn
│ ├── src
│ │ ├── main
│ │ │ ├── docker
│ │ │ └── java
│ │ │ └── org
│ │ │ └── acme
│ │ │ ├── controllers
│ │ │ ├── entities
│ │ │ ├── Generics
│ │ │ ├── pagination
│ │ └── test
│ ├── target
│ ├── Dockerfile
│ ├── pom.xml
|
├── front
│ ├── node_modules
│ ├── public
│ ├── src
│ │ ├── assets
│ │ │ ├── fonts
│ │ │ └── icons
│ │ ├── components
│ │ ├── styles
│ │ └── utils
│ ├── .eslint.cjs
│ ├── Dockerfile
│ ├── package-lock.json
│ ├── package.json
│ └── vite.config.js
└── ...
```


## 要求

- Node.js（用于前端）
- Maven（用于后端）
- Java JDK 21 或更高版本（用于后端）

## 配置和运行

### 前端（React）

1. 导航到 `front` 目录：

   ```bash
   cd front
   ```


2. 安装前端依赖：

   ```bash
   npm install
   ```


3. 运行：

   ```bash
   npm run dev
   ```


### 后端（Quarkus）

React 应用将运行在 `http://localhost:8080`。

1. 导航到 `back` 目录：

   ```bash
   cd back
   ```


2. 运行应用：

   ```bash
   ./mvnw quarkus:dev
   ```


Quarkus 服务器将运行在 `http://localhost:8080`。

### 数据库

该项目使用关系型数据库postgres

### API 端点

后端暴露了多个 RESTful 端点。你可以通过浏览器或 Postman 等工具访问这些端点。以下是一些示例端点：

- 获取所有学生信息：`GET /api/students`
- 添加新学生：`POST /api/students`
- 更新学生信息：`PUT /api/students/{id}`
- 删除学生：`DELETE /api/students/{id}`

