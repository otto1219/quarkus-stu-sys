package com.gec.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gec.entities.Teacher;
import com.gec.pagination.PagedResult;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Page;
import io.quarkus.panache.common.Sort;
import jakarta.annotation.security.RolesAllowed;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import org.jboss.logging.Logger;

import java.util.*;

import static com.gec.Generics.GenericHelper.updateEntity;

@Path("teacher")
@ApplicationScoped
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RolesAllowed({"VIEW_ADMIN_DETAILS"})
public class TeacherController {

    private static final Logger LOGGER = Logger.getLogger(TeacherController.class.getName());

    /**
     * 列出所有教师信息，支持分页和排序功能。
     *
     * @param page     页码，从0开始。
     * @param size     每页大小。
     * @param sort     排序字段。
     * @param teacherName   教师姓名的查询条件。
     * @param subjectTaught 教授科目的查询条件。
     * @param schoolId      学校ID的查询条件。
     * @return 包含教师信息的分页结果。
     */
    @GET
    public Response listAll(
            @QueryParam("page") Integer page,
            @QueryParam("size") Integer size,
            @QueryParam("sort") String sort,
            @QueryParam("teacherName") String teacherName,
            @QueryParam("subjectTaught") String subjectTaught,
            @QueryParam("schoolId") Long schoolId
    ) {
        // 初始化分页和排序参数
        size = (size == null) ? 10 : size;
        page = (page == null) ? 0 : page;
        sort = (Objects.equals(sort, "")) ? "teacherName" : sort;

        // 确保排序字段有效
        String validSortColumn = "teacherName";
        List<String> validSortColumns = Arrays.asList("teacherName", "subjectTaught", "schoolId");
        if (validSortColumns.contains(sort)) {
            validSortColumn = sort;
        }

        // 构建基础查询语句
        String queryStr = "FROM Teacher WHERE 1=1";
        Map<String, Object> params = new HashMap<>();

        // 根据查询条件构建查询语句
        if (teacherName != null && !teacherName.isEmpty()) {
            queryStr += " AND LOWER(teacherName) LIKE LOWER(CONCAT('%', :teacherName, '%'))";
            params.put("teacherName", teacherName);
        }
        if (subjectTaught != null && !subjectTaught.isEmpty()) {
            queryStr += " AND LOWER(subjectTaught) LIKE LOWER(CONCAT('%', :subjectTaught, '%'))";
            params.put("subjectTaught", subjectTaught);
        }
        if (schoolId != null) {
            queryStr += " AND school.id = :schoolId";
            params.put("schoolId", schoolId);
        }

        // 执行查询
        PanacheQuery<Teacher> query = Teacher.find(queryStr, Sort.by(validSortColumn), params);
        Long totalCount = query.count();
        List<Teacher> teachers = query.page(Page.of(page, size)).list();

        // 构建并返回分页结果
        return Response.ok(
                new PagedResult<>(
                        teachers,
                        totalCount,
                        (int) Math.ceil((double) totalCount / size),
                        page > 0,
                        (page + 1) * size < totalCount
                )
        ).build();
    }

    /**
     * 获取单个教师的详细信息。
     *
     * @param id 教师的ID。
     * @return 教师实体对象。
     * @throws WebApplicationException 如果指定ID的教师不存在。
     */
    @Path("{id}")
    public Teacher getSingle(Long id) {
        Teacher entity = Teacher.findById(id);
        if (entity == null) {
            throw new WebApplicationException("Teacher with id of " + id + " does not exist.", 404);
        }
        return entity;
    }

    /**
     * 创建新的教师实体。
     *
     * @param entity 新的教师实体对象。
     * @return 创建成功后的教师实体对象。
     * @throws WebApplicationException 如果请求体中的ID非法设置。
     */
    @POST
    @Transactional
    public Response create(Teacher entity) {
        if (entity.id != null) {
            throw new WebApplicationException("Id was invalidly set on request.", 422);
        }

        entity.persist();
        return Response.ok(entity).status(201).build();
    }

    /**
     * 更新教师实体信息。
     *
     * @param id 教师的ID。
     * @param updatedTeacher 更新后的教师实体对象。
     * @return 更新成功后的教师实体对象。
     * @throws WebApplicationException 如果指定ID的教师不存在。
     */
    @PUT
    @Path("{id}")
    @Transactional
    public Response updateTeacher(@PathParam("id") Long id, Teacher updatedTeacher) {
        Teacher existingTeacher = Teacher.findById(id);
        if (existingTeacher == null) {
            throw new WebApplicationException("Teacher with id " + id + " does not exist.", 404);
        }

        updateEntity(existingTeacher, updatedTeacher);

        return Response.ok(existingTeacher).build();
    }

    /**
     * 删除教师实体。
     *
     * @param id 教师的ID。
     * @return 删除操作的结果响应。
     * @throws WebApplicationException 如果指定ID的教师不存在。
     */
    @DELETE
    @Path("{id}")
    @Transactional
    public Response delete(Long id) {
        Teacher entity = Teacher.findById(id);
        if (entity == null) {
            throw new WebApplicationException("Teacher with id of " + id + " does not exist.", 404);
        }
        entity.delete();
        return Response.status(204).build();
    }

    /**
     * ErrorMapper 类用于全局异常处理，将未捕获的异常映射为适当的HTTP响应。
     */
    @Provider
    public static class ErrorMapper implements ExceptionMapper<Exception> {

        @Inject
        ObjectMapper objectMapper;

        /**
         * 将异常转换为HTTP响应。
         *
         * @param exception 需要处理的异常。
         * @return 包含异常信息的HTTP响应。
         */
        @Override
        public Response toResponse(Exception exception) {
            LOGGER.error("Failed to handle request", exception);

            int code = 500;
            if (exception instanceof WebApplicationException) {
                code = ((WebApplicationException) exception).getResponse().getStatus();
            }

            ObjectNode exceptionJson = objectMapper.createObjectNode();
            exceptionJson.put("exceptionType", exception.getClass().getName());
            exceptionJson.put("code", code);

            if (exception.getMessage() != null) {
                exceptionJson.put("error", exception.getMessage());
            }

            return Response.status(code)
                    .entity(exceptionJson)
                    .build();
        }

    }
}
