package com.gec.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gec.Generics.GenericHelper;
import com.gec.entities.Enrollment;
import com.gec.pagination.PagedResult;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Page;
import io.quarkus.panache.common.Sort;
import jakarta.annotation.security.RolesAllowed;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import org.jboss.logging.Logger;

import java.sql.Timestamp;
import java.util.*;

@Path("enrollment")
@ApplicationScoped
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RolesAllowed({"VIEW_ADMIN_DETAILS"})
public class EnrollmentController {
    // 日志记录器，用于记录日志信息。
    private static final Logger LOGGER = Logger.getLogger(EnrollmentController.class.getName());
    @GET
    public Response listAll(
            @QueryParam("page") Integer page,
            @QueryParam("size") Integer size,
            @QueryParam("sort") String sort,
            @QueryParam("studentId") Long studentId,
            @QueryParam("enrollmentDate") Timestamp enrollmentDate
    ) {
        // 初始化默认的分页和排序参数
        size = (size == null) ? 10 : size;
        page = (page == null) ? 0 : page;
        sort = (Objects.equals(sort, "")) ? "enrollmentDate" : sort;

        // 确保排序字段有效
        String validSortColumn = "enrollmentDate";
        List<String> validSortColumns = Arrays.asList("enrollmentDate", "studentId");
        if (validSortColumns.contains(sort)) {
            validSortColumn = sort;
        }

        // 构建基础查询字符串
        String queryStr = "FROM Enrollment WHERE 1=1";
        Map<String, Object> params = new HashMap<>();

        // 根据学生ID和注册日期构建查询条件
        if (studentId != null) {
            queryStr += " AND student.id = :studentId";
            params.put("studentId", studentId);
        }
        if (enrollmentDate != null) {
            queryStr += " AND enrollmentDate = :enrollmentDate";
            params.put("enrollmentDate", enrollmentDate);
        }

        // 创建带有累积参数的查询
        PanacheQuery<Enrollment> query = Enrollment.find(queryStr, Sort.by(validSortColumn), params);

        // 计算总记录数和分页信息
        Long totalCount = query.count();
        List<Enrollment> enrollments = query.page(Page.of(page, size)).list();

        // 构建并返回分页结果
        return Response.ok(
                new PagedResult<>(
                        enrollments,
                        totalCount,
                        (int) Math.ceil((double) totalCount / size),
                        page > 0,
                        (page + 1) * size < totalCount
                )
        ).build();
    }

    /**
     * 获取单个注册记录。
     *
     * @param id  注册记录的ID
     * @return 包含单个注册记录的响应
     */
    @Path("{id}")
    public Enrollment getSingle(Long id) {
        Enrollment entity = Enrollment.findById(id);
        if (entity == null) {
            throw new WebApplicationException("Enrollment with id of " + id + " does not exist.", 404);
        }
        return entity;
    }

    /**
     * 创建新的注册记录。
     *
     * @param entity  新的注册记录实体
     * @return 包含新创建注册记录的响应
     */
    @POST
    @Transactional
    public Response create(Enrollment entity) {
        if (entity.id != null) {
            throw new WebApplicationException("Id was invalidly set on request.", 422);
        }

        entity.persist();
        return Response.ok(entity).status(201).build();
    }

    /**
     * 更新注册记录。
     *
     * @param id  注册记录的ID
     * @param updatedEnrollment  更新后的注册记录实体
     * @return 包含更新后注册记录的响应
     */
    @PUT
    @Path("{id}")
    @Transactional
    public Response updateEnrollment(@PathParam("id") Long id, Enrollment updatedEnrollment) {
        Enrollment existingEnrollment = Enrollment.findById(id);
        if (existingEnrollment == null) {
            throw new WebApplicationException("Enrollment with id " + id + " does not exist.", 404);
        }

        GenericHelper.updateEntity(existingEnrollment, updatedEnrollment);

        return Response.ok(existingEnrollment).build();
    }

    /**
     * 删除注册记录。
     *
     * @param id  注册记录的ID
     * @return 表示删除成功的响应
     */
    @DELETE
    @Path("{id}")
    @Transactional
    public Response delete(Long id) {
        Enrollment entity = Enrollment.findById(id);
        if (entity == null) {
            throw new WebApplicationException("Enrollment with id of " + id + " does not exist.", 404);
        }
        entity.delete();
        return Response.status(204).build();
    }

    /**
     * 全局异常处理映射器，将异常转换为HTTP响应。
     */
    @Provider
    public static class ErrorMapper implements ExceptionMapper<Exception> {

        @Inject
        ObjectMapper objectMapper;

        @Override
        public Response toResponse(Exception exception) {
            LOGGER.error("Failed to handle request", exception);

            int code = 500;
            if (exception instanceof WebApplicationException) {
                code = ((WebApplicationException) exception).getResponse().getStatus();
            }

            ObjectNode exceptionJson = objectMapper.createObjectNode();
            exceptionJson.put("exceptionType", exception.getClass().getName());
            exceptionJson.put("code", code);

            if (exception.getMessage() != null) {
                exceptionJson.put("error", exception.getMessage());
            }

            return Response.status(code)
                    .entity(exceptionJson)
                    .build();
        }

    }
}
