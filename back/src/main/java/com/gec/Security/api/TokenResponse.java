package com.gec.Security.api;

public record TokenResponse(String token, String expiresIn){
}
