package com.gec.Security.users.errors;

import lombok.Getter;

import java.io.Serializable;

@Getter
public abstract class DomainDataException extends RuntimeException {

    private final Serializable entity;

    protected DomainDataException(final String message, final Serializable entity) {
        super(message);
        this.entity = entity;
    }

}
